﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "BitConverter Client";
            while (true)
            {
                Console.Write("Press enter to start sending");
                Console.ReadLine();
                Socket client = new Socket(SocketType.Stream, ProtocolType.Tcp);
                client.Connect(IPAddress.Loopback, 1308);

                bool aBool = true;
                char aChar = 'A';
                double aDouble = 10.01;
                int anInt = 100;
                long aLong = 1000000;
                short aShort = 256;
                List<byte> bytes = new List<byte>();
                bytes.AddRange(BitConverter.GetBytes(aBool));
                Console.WriteLine($"bool: {aBool}, {sizeof(bool)} byte(s)");
                bytes.AddRange(BitConverter.GetBytes(aChar));
                Console.WriteLine($"char: {aChar}, {sizeof(char)} byte(s)");
                bytes.AddRange(BitConverter.GetBytes(aDouble));
                Console.WriteLine($"double: {aDouble}, {sizeof(double)} byte(s)");
                bytes.AddRange(BitConverter.GetBytes(anInt));
                Console.WriteLine($"int: {anInt}, {sizeof(int)} byte(s)");
                bytes.AddRange(BitConverter.GetBytes(aLong));
                Console.WriteLine($"long: {aLong}, {sizeof(long)} byte(s)");
                bytes.AddRange(BitConverter.GetBytes(aShort));
                Console.WriteLine($"short: {aShort}, {sizeof(short)} byte(s)");
                client.Send(bytes.ToArray());
                client.Close();
            }
        }
    }
}
