﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "BitConveter Server";
            Socket listener = new Socket(SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(new IPEndPoint(IPAddress.Any, 1308));
            listener.Listen(10);
            while (true)
            {
                Socket client = listener.Accept();
                byte[] buffer = new byte[8];

                // đọc 1 byte đầu tiên và chuyển thành biến bool
                client.Receive(buffer, 1, SocketFlags.None);
                bool aBool = BitConverter.ToBoolean(buffer, 0);
                Console.WriteLine($"bool: {aBool}");

                // đọc 2 byte tiếp theo chuyển thành char
                client.Receive(buffer, 2, SocketFlags.None);
                char aChar = BitConverter.ToChar(buffer, 0);
                Console.WriteLine($"char: {aChar}");

                // đọc 8 byte tiếp theo chuyển thành double
                client.Receive(buffer, 8, SocketFlags.None);
                double aDouble = BitConverter.ToDouble(buffer, 0);
                Console.WriteLine($"double: {aDouble}");

                // đọc 4 byte tiếp theo chuyển thành int
                client.Receive(buffer, 4, SocketFlags.None);
                int anInt = BitConverter.ToInt32(buffer, 0);
                Console.WriteLine($"Int: {anInt}");

                // đọc 8 byte tiếp theo chuyển thành long
                client.Receive(buffer, 8, SocketFlags.None);
                long aLong = BitConverter.ToInt64(buffer, 0);
                Console.WriteLine($"long: {aLong}");

                // đọc 2 byte tiếp theo chuyển thành short
                client.Receive(buffer, 2, SocketFlags.None);
                short aShort = BitConverter.ToInt16(buffer, 0);
                Console.WriteLine($"short: {aShort}");
                client.Close();
            }
        }
    }
}
